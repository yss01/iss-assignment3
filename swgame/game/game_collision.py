from game_config import *


def stationary_collision(px, py, lx, ly):
    player_rect = x_wing_image_rect
    player_rect.center = (px, py)

    for i, val in enumerate(lx):
        obstacle_rect = asteroid_rect
        obstacle_rect.x = val
        obstacle_rect.y = ly[i]

        if player_rect.colliderect(obstacle_rect):
            return True

    return False


def moving_collision(enemy_pos):
    gap = 150
    for i, val in enumerate(enemy_pos):
        check_rect = tie_image_rect
        check_rect.center = (val, gap)
        if x_wing_image_rect.colliderect(check_rect):
            return True
            break
        gap += 2 * lane_height

    return False
