from game_config import *
import game_utilities
import sys


def objective_page():
    pygame.mixer.Channel(theme_channel).pause()
    obj_var = False

    while not obj_var:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        screen.blit(obj_background, [0, 0])
        mouse = pygame.mouse.get_pos()
        font_button = pygame.font.Font(font_button_type, 40)

        if 20 + mb_w-150 > mouse[0] > 20 and 20 + 0.75*mb_h > mouse[1] > 20:
            game_utilities.button_action("Back", font_button,
                                         yellow, black, 20,
                                         20, mb_w - 150, 0.75 * mb_h, 20 +
                                         (mb_w - 150)/2, 20 +
                                         (0.75 * mb_h)/2, 3)
        else:
            game_utilities.button_action("Back", font_button,
                                         black, yellow, 20,
                                         20, mb_w - 150, 0.75 * mb_h, 20 +
                                         (mb_w - 150)/2, 20 +
                                         (0.75 * mb_h)/2, 0)

        leia_image_rect.center = (display_w - leia_w, leia_h)
        screen.blit(leia_image, leia_image_rect)

        game_utilities.paragraph("Transmission from Leia Organa:",
                                 100, 150, font_content_type, 30, 0)
        game_utilities.paragraph("THE REBELLION NEEDS YOU!", 100,
                                 200, font_content_type, 80, 0)
        game_utilities.paragraph("As the evil Galactic Empire expands "
                                 "its power,", 100, 300,
                                 font_content_type, 40, 0)
        game_utilities.paragraph("more and more pilots have lost their "
                                 "lives in combats", 100, 350,
                                 font_content_type, 40, 0)
        game_utilities.paragraph("against the tie fighters and the menacing "
                                 "Star Destroyers.", 100, 400,
                                 font_content_type, 40, 0)
        game_utilities.paragraph("You must pass the three levels of the test",
                                 100, 500, font_content_type, 40, 0)
        game_utilities.paragraph("in order to join the Resistance.", 100,
                                 550, font_content_type, 40, 0)
        game_utilities.paragraph("We really hope to see you in the "
                                 "fleet, pilot.",
                                 100, 600, font_content_type, 40, 0)
        game_utilities.paragraph("When you are ready, click the Start button.",
                                 100, 650, font_content_type, 40, 0)
        game_utilities.paragraph("Until then,", 100, 750,
                                 font_content_type, 40, 0)
        game_utilities.paragraph("MAY THE FORCE BE WITH YOU.", 100,
                                 800, font_content_type, 60, 0)

        if display_w/2 + mb_w/2 > mouse[0] > display_w/2 - mb_w/2 and \
                900 + mb_h > mouse[1] > 900:
            game_utilities.button_action("Start", font_button, yellow,
                                         black, display_w/2 - mb_w/2, 900,
                                         mb_w, mb_h, display_w/2,
                                         900 + mb_h/2, 4)
        else:
            game_utilities.button_action("Start", font_button, black, yellow,
                                         display_w / 2 - mb_w / 2, 900,
                                         mb_w, mb_h, display_w / 2,
                                         900 + mb_h / 2, 0)

        pygame.display.update()
        clock.tick(60)
