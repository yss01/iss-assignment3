from game_config import *
import pygame
import random
import game_collision
import game_utilities
import sys


def player(px, py):
    x_wing_image_rect.center = (px, py)
    screen.blit(x_wing_image, x_wing_image_rect)


def enemy(ex, ey):
    tie_image_rect.center = (ex, ey)
    screen.blit(tie_image, tie_image_rect)


def loop_page(pos_x, pos_y, list_x, list_y, level,
              player1_chances, player2_chances, turn,
              p1f_level, p2f_level, p1_points, p2_points):
    crossed = [0, 0, 0, 0]
    enemy_pos = []
    for i in range(1, 9):
        if i % 2 == 1:
            enemy_pos.append(random.randint(obstacle_start_x +
                                            item_size/2,
                                            display_w - obstacle_start_x -
                                            item_size/2 + 1))

    loop_var = True
    change_x = 0
    change_y = 0
    enemy_change = []
    start_time = pygame.time.get_ticks()
    for i in range(0, 4):
        enemy_change.append(level * enemy_speed)
    while loop_var:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT or event.key == pygame.K_a:
                    change_x = -1 * speed
                if event.key == pygame.K_RIGHT or event.key == pygame.K_d:
                    change_x = speed
                if event.key == pygame.K_DOWN or event.key == pygame.K_s:
                    change_y = speed
                if event.key == pygame.K_UP or event.key == pygame.K_w:
                    change_y = -1 * speed
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or \
                        event.key == pygame.K_a or \
                        event.key == pygame.K_RIGHT or \
                        event.key == pygame.K_d:
                    change_x = 0
                if event.key == pygame.K_UP or \
                        event.key == pygame.K_w or \
                        event.key == pygame.K_DOWN or \
                        event.key == pygame.K_s:
                    change_y = 0
        if level == 1:
            map_image = pygame.image.load('../images/map1.jpg')
        elif level == 2:
            map_image = pygame.image.load('../images/map2.jpg')
        else:
            map_image = pygame.image.load('../images/map3.jpg')
        screen.blit(map_image, [0, 0])

        cur_time = (pygame.time.get_ticks() - start_time) / 1000
        game_utilities.scoreboard(turn, level,
                                  p1_points, p2_points, cur_time)

        pos_x += change_x
        pos_y += change_y

        if pos_x < obstacle_start_x + item_size/2:
            pos_x = obstacle_start_x + item_size/2
        elif pos_x > display_w - obstacle_start_x - item_size/2:
            pos_x = display_w - obstacle_start_x - item_size/2

        if pos_y > 10 * lane_height - item_size/2:
            pos_y = 10 * lane_height - item_size/2
        elif pos_y < item_size/2:
            pos_y = item_size/2

        for i, val in enumerate(enemy_pos):
            enemy_pos[i] += enemy_change[i]
            if enemy_pos[i] <= obstacle_start_x + item_size/2 \
                    or enemy_pos[i] >= display_w -\
                    obstacle_start_x - item_size / 2:
                enemy_change[i] = -1 * enemy_change[i]

        collided = False

        if game_collision.moving_collision(enemy_pos) == 1 or \
                game_collision.stationary_collision(pos_x, pos_y,
                                                    list_x, list_y) == 1:
            collided = True

        if collided:
            game_utilities.chance_end(0, list_x, list_y, level,
                                      player1_chances, player2_chances,
                                      turn, p1f_level, p2f_level,
                                      p1_points, p2_points)

        if pos_y <= 2*lane_height + item_size/2 and crossed[1] == 0:
            crossed[1] = 1
            if turn == 1:
                p1_points += 50.0/abs(pos_x - enemy_pos[3])
            else:
                p2_points += 50.0/abs(pos_x - enemy_pos[3])

        if pos_y <= 4*lane_height + item_size/2 and crossed[2] == 0:
            crossed[2] = 1
            if turn == 1:
                p1_points += 50.0/abs(pos_x - enemy_pos[2])
            else:
                p2_points += 50.0/abs(pos_x - enemy_pos[2])

        if pos_y <= 6*lane_height + item_size/2 and crossed[3] == 0:
            crossed[3] = 1
            if turn == 1:
                p1_points += 50.0/abs(pos_x - enemy_pos[1])
            else:
                p2_points += 50.0/abs(pos_x - enemy_pos[1])

        if pos_y == item_size/2:
            if turn == 1:
                p1_points += 50.0/abs(pos_x - enemy_pos[0])
            else:
                p2_points += 50.0/abs(pos_x - enemy_pos[0])
            game_utilities.chance_end(1, list_x, list_y, level,
                                      player1_chances, player2_chances,
                                      turn, p1f_level, p2f_level,
                                      p1_points, p2_points)
        player(pos_x, pos_y)

        gap = lane_height + item_size/2

        for val in enemy_pos:
            enemy(val, gap)
            gap += 2 * lane_height

        pygame.display.update()
        clock.tick(60)
