from game_config import *
import pygame
import game_help
import game_intro
import game_objective
import game_environment
import game_loop
import game_verdict
import sys


def scoreboard(turn, level, p1_points, p2_points, cur_time):
    paragraph("Player ", 25, 20, font_content_type, 30, 0)
    paragraph(str(turn), 125, 20, font_content_type, 30, 0)
    paragraph("Level ", display_w - 150, 20, font_content_type, 30, 0)
    paragraph(str(level), display_w - 50, 20, font_content_type, 30, 0)
    if turn == 1:
        paragraph(str(round(p1_points*1000)), display_w/2 - 100,
                  30, font_content_type, 30, 1)
    else:
        paragraph(str(round(p2_points*1000)), display_w/2 - 100,
                  30, font_content_type, 30, 1)

    paragraph(str(round(cur_time)), display_w/2 + 100, 30,
              font_content_type, 30, 1)
    paragraph("sec", display_w/2 + 150, 30,
              font_content_type, 30, 1)


def chance_end(res, list_x, list_y, level, player1_chances,
               player2_chances, turn,
               p1f_level, p2f_level, p1_points, p2_points):

    current_time = pygame.time.get_ticks()
    exit_time = current_time + delay
    chance_var = False
    if res == 0:
        pygame.mixer.music.load(explosion_sound)
        pygame.mixer.music.play(0)
    else:
        pygame.mixer.music.load(victory_sound)
        pygame.mixer.music.play(0)

    if res == 1 and level != 3:
        if turn == 1:
            p1f_level += 1
        else:
            p2f_level += 1

    p1_chance_update = player1_chances - 1
    p2_chance_update = player2_chances - 1
    while not chance_var:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
        screen.blit(background_image, [0, 0])
        pygame.draw.rect(screen, black, (display_w/2 - message_x/2,
                                         display_h/2 - message_y/2,
                                         message_x, message_y))
        if res == 0:
            paragraph("The Rebellion", display_w/2 - message_x/2 + 75,
                      display_h/2 - message_y/2 + 100,
                      font_content_type, 50, 0)
            paragraph("IS CRUSHED!", display_w/2 - message_x/2 + 75,
                      display_h/2 - message_y/2 + 200,
                      font_content_type, 100, 0)

            vader_image_rect.center = (display_w - vader_w, vader_h)
            screen.blit(vader_image, vader_image_rect)
        else:
            paragraph("Death to the", display_w/2 - message_x/2 + 75,
                      display_h / 2 - message_y / 2 + 100,
                      font_content_type, 50, 0)
            paragraph("EMPIRE!", display_w/2 - message_x/2 + 75,
                      display_h/2 - message_y/2 + 200,
                      font_content_type, 100, 0)

            luke_image_rect.center = (display_w - luke_w, luke_h)
            screen.blit(luke_image, luke_image_rect)
        paragraph("Loading...", display_w/2 - message_x/2 + 75,
                  display_h/2 - message_y/2 + 350,
                  font_content_type, 30, 0)

        if res == 1:
            if level == 3:
                if turn == 1:
                    p1f_level = level
                    current_time = pygame.time.get_ticks()
                    if current_time >= exit_time:
                        game_loop.loop_page(initial_pos_x, initial_pos_y,
                                            list1_x, list1_y, 1,
                                            player1_chances, player2_chances,
                                            2, p1f_level, p2f_level,
                                            p1_points, p2_points)

                else:
                    p2f_level = level
                    current_time = pygame.time.get_ticks()
                    if current_time >= exit_time:
                        paragraph("Move your mouse.", display_w / 2 -
                                  message_x / 2 + 75,
                                  display_h / 2 - message_y / 2 + 400,
                                  font_content_type, 30, 0)
                        game_verdict.verdict_page(p1f_level, p2f_level,
                                                  p1_points, p2_points)
                        game_verdict.verdict_page(p1f_level, p2f_level,
                                                  p1_points, p2_points)
            else:
                current_time = pygame.time.get_ticks()
                if current_time >= exit_time:
                    if level == 1:
                        game_loop.loop_page(initial_pos_x, initial_pos_y,
                                            list2_x, list2_y,
                                            2, player1_chances,
                                            player2_chances, turn, p1f_level,
                                            p2f_level, p1_points, p2_points)
                    elif level == 2:
                        game_loop.loop_page(initial_pos_x, initial_pos_y,
                                            list3_x, list3_y,
                                            3, player1_chances,
                                            player2_chances, turn, p1f_level,
                                            p2f_level, p1_points, p2_points)
        else:
            if turn == 1:
                if player1_chances > 1:
                    current_time = pygame.time.get_ticks()
                    if current_time >= exit_time:
                        game_loop.loop_page(initial_pos_x, initial_pos_y,
                                            list_x, list_y, level,
                                            p1_chance_update,
                                            player2_chances, turn,
                                            p1f_level, p2f_level,
                                            p1_points, p2_points)
                else:
                    current_time = pygame.time.get_ticks()
                    if current_time >= exit_time:
                        game_loop.loop_page(initial_pos_x, initial_pos_y,
                                            list1_x, list1_y, 1,
                                            player1_chances, 3, 2,
                                            p1f_level, p2f_level,
                                            p1_points, p2_points)
            else:
                if player2_chances > 1:
                    current_time = pygame.time.get_ticks()
                    if current_time >= exit_time:
                        game_loop.loop_page(initial_pos_x, initial_pos_y,
                                            list_x, list_y, level,
                                            player1_chances,
                                            p2_chance_update, turn,
                                            p1f_level, p2f_level,
                                            p1_points, p2_points)
                else:
                    current_time = pygame.time.get_ticks()
                    if current_time >= exit_time:
                        paragraph("Move your mouse.", display_w / 2 -
                                  message_x / 2 + 75,
                                  display_h / 2 - message_y / 2 + 400,
                                  font_content_type, 30, 0)
                        game_verdict.verdict_page(p1f_level, p2f_level,
                                                  p1_points, p2_points)

        pygame.display.update()
        clock.tick(10)


def button_action(text_content, f, color1, color2, button_x, button_y,
                  bw, bh, text_x, text_y, flag):
    click = pygame.mouse.get_pressed()
    pygame.draw.rect(screen, color2, (button_x, button_y, bw, bh))

    text = f.render(text_content, True, color1)
    text_rect = text.get_rect()
    text_rect.center = (text_x, text_y)
    screen.blit(text, text_rect)

    if flag == 1:
        if click[0] == 1:
            game_objective.objective_page()
    elif flag == 2:
        if click[0] == 1:
            game_help.help_page()
    elif flag == 3:
        if click[0] == 1:
            game_intro.intro_page()
    elif flag == 4:
        if click[0] == 1:
            game_environment.set_environment(1)
            game_environment.set_environment(2)
            game_environment.set_environment(3)
    elif flag == 5:
        if click[0] == 1:
            sys.exit()


def paragraph(content, w, h, f, size, c):
    font_help = pygame.font.Font(f, size)

    content_text = font_help.render(content, True, yellow)
    content_text_rect = content_text.get_rect()
    if c == 1:
        content_text_rect.center = (w, h)
    else:
        content_text_rect.x = w
        content_text_rect.y = h
    screen.blit(content_text, content_text_rect)
