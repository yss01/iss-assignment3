from game_config import *
import pygame
import sys
import game_utilities


def verdict_page(p1f_level, p2f_level, p1_points, p2_points):
    verdict_var = False

    p1_points = round(p1_points*1000)
    p2_points = round(p2_points*1000)

    while not verdict_var:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            screen.blit(obj_background, [0, 0])
            mouse = pygame.mouse.get_pos()
            font_button = pygame.font.Font(font_button_type, 40)

            if 20 + mb_w - 150 > mouse[0] > 20 and 20 + \
                    0.75 * mb_h > mouse[1] > 20:
                game_utilities.button_action("Exit", font_button, yellow,
                                             black, 20, 20, mb_w - 150,
                                             0.75 * mb_h, 20 +
                                             (mb_w - 150) / 2, 20 +
                                             (0.75 * mb_h) / 2, 5)
            else:
                game_utilities.button_action("Exit", font_button,
                                             black, yellow,
                                             20, 20, mb_w - 150,
                                             0.75 * mb_h, 20 +
                                             (mb_w - 150) / 2, 20 +
                                             (0.75 * mb_h) / 2, 0)

            han_image_rect.center = (display_w - han_w, han_h)
            screen.blit(han_image, han_image_rect)
            if p2f_level > p1f_level:
                winner = "Player 2"
            if p2f_level < p1f_level:
                winner = "Player 1"
            if p2f_level == p1f_level:
                if p1_points > p2_points:
                    winner = "Player 1"
                elif p2_points > p1_points:
                    winner = "Player 2"
                else:
                    winner = "none"

            game_utilities.paragraph("Transmission from Han Solo:", 100,
                                     150, font_content_type, 30, 0)
            if winner != "none":
                if winner == "Player 1":
                    game_utilities.paragraph("GREAT FLYING, Player 1!",
                                             100, 200,
                                             font_content_type, 80, 0)
                else:
                    game_utilities.paragraph("GREAT FLYING, Player 2!",
                                             100, 200,
                                             font_content_type, 80, 0)
                game_utilities.paragraph("Good Luck for your adventures",
                                         100, 300,
                                         font_content_type, 40, 0)
                game_utilities.paragraph("with The Resistance!",
                                         100, 350,
                                         font_content_type, 40, 0)
            else:
                game_utilities.paragraph("It seems as if",
                                         100, 200,
                                         font_content_type, 40, 0)
                game_utilities.paragraph("both of you are equally good!",
                                         100, 250,
                                         font_content_type, 40, 0)

            game_utilities.paragraph("Final Results:", 100, 500,
                                     font_content_type, 40, 0)
            game_utilities.paragraph("Player 1 Points:", 100, 550,
                                     font_content_type, 40, 0)
            game_utilities.paragraph(str(p1_points), 500, 550,
                                     font_content_type, 40, 0)
            game_utilities.paragraph("Player 2 Points:", 100, 600,
                                     font_content_type, 40, 0)
            game_utilities.paragraph(str(p2_points), 500, 600,
                                     font_content_type, 40, 0)

        pygame.display.update()
        clock.tick(60)
