from game_config import *
import random
import game_loop
import pygame


def set_environment(j):
    screen.blit(game_background, [0, 0])
    pos = 0
    gap = 100
    for i in range(1, 9):
        x_distance = obstacle_start_x
        if i % 2 == 0:
            while x_distance <= 1900:
                pos += 1
                c = random.randint(1, 2)
                asteroid_rect.x = x_distance
                asteroid_rect.y = gap
                if c == 1:
                    screen.blit(asteroid, asteroid_rect)
                    if j == 1:
                        list1_x.append(x_distance)
                        list1_y.append(gap)
                    elif j == 2:
                        list2_x.append(x_distance)
                        list2_y.append(gap)
                    else:
                        list3_x.append(x_distance)
                        list3_y.append(gap)

                x_distance += item_size
        gap += lane_height
    if j == 1:
        pygame.image.save(screen, "../images/map1.jpg")
    elif j == 2:
        pygame.image.save(screen, "../images/map2.jpg")
    else:
        pygame.image.save(screen, "../images/map3.jpg")

    level = 1
    player1_chances = 3
    player2_chances = 3
    turn = 1
    p1f_level = 0
    p2f_level = 0
    p1_points = 0
    p2_points = 0

    if j == 3:
        game_loop.loop_page(initial_pos_x, initial_pos_y,
                            list1_x, list1_y, level,
                            player1_chances, player2_chances, turn,
                            p1f_level, p2f_level, p1_points, p2_points)

