import pygame
from game_main import *


def intro():
    game_over = False

    while not game_over:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over = True

        screen.blit(logo_image, logo_image_rect)

        pygame.display.update()
        clock.tick(60)
