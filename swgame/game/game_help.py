from game_config import *
import pygame
import sys
import game_utilities


def help_page():
    help_var = False

    while not help_var:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        screen.blit(help_background, [0, 0])

        mouse = pygame.mouse.get_pos()
        font_button = pygame.font.Font(font_button_type, 40)

        if 20 + mb_w-150 > mouse[0] > 20 and 20 + 0.75*mb_h > mouse[1] > 20:
            game_utilities.button_action("Back", font_button, yellow, black, 20,
                                         20, mb_w - 150, 0.75 * mb_h, 20 +
                                         (mb_w - 150)/2,
                                         20 + (0.75 * mb_h)/2, 3)
        else:
            game_utilities.button_action("Back", font_button, black, yellow, 20,
                                         20, mb_w - 150, 0.75 * mb_h, 20 +
                                         (mb_w - 150)/2,
                                         20 + (0.75 * mb_h)/2, 0)

        game_utilities.paragraph("in this two-player game, only one player "
                                 "plays at a time.", display_w/2, 200,
                                 font_help_type, 40, 1)
        game_utilities.paragraph("there are three levels for each player,",
                                 display_w/2, 275, font_help_type, 40, 1)
        game_utilities.paragraph("and only three chances to reach as "
                                 "far as possible.", display_w/2, 350,
                                 font_help_type, 40, 1)
        game_utilities.paragraph("the order to determine the winners "
                                 "is as follows:", display_w/2,
                                 425, font_help_type, 40, 1)
        game_utilities.paragraph("1. the number of levels completed",
                                 display_w/2, 500, font_help_type, 40, 1)
        game_utilities.paragraph("2. total number of points", display_w/2,
                                 575, font_help_type, 40, 1)
        game_utilities.paragraph("the x-wing can be navigated using "
                                 "arrow keys or w a s d.",
                                 display_w/2, 650, font_help_type, 40, 1)
        game_utilities.paragraph("variable points are awarded when the "
                                 "player avoids a moving obstacle.", display_w/2,
                                 725, font_help_type, 40, 1)
        game_utilities.paragraph("higher the risk in avoiding an "
                                 "obstacle, higher the points.",
                                 display_w/2, 800,
                                 font_help_type, 40, 1)
        game_utilities.paragraph("The Star Wars franchise is the "
                                 "brainchild of George Lucas.",
                                 display_w/2, 900,
                                 font_content_type, 20, 1)
        game_utilities.paragraph("Music composed by John Williams.",
                                 display_w/2, 925, font_content_type,
                                 20, 1)

        pygame.display.update()
        clock.tick(60)
