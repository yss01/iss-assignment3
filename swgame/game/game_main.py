from game_config import *
import game_intro
import pygame
import sys

pygame.init()

pygame.display.set_caption('A Star Wars Game')

main_music = pygame.mixer.Channel(theme_channel)
main_music.play(pygame.mixer.Sound(theme_song), loops=-1)
game_intro.intro_page()
pygame.quit()
sys.exit()
