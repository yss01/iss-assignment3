import pygame

# colors
yellow = (230, 193, 56)
black = (0, 0, 0)
white = (255, 255, 255)
grey = (128, 128, 128)

# display
display_w = 1920
display_h = 1080
leia_w = 325
leia_h = display_h/1.25 - 200
vader_w = 550
vader_h = display_h/1.5 - 150
luke_w = 550
luke_h = display_h/1.5 - 175
han_w = 300
han_h = display_h/1.25 - 200

# audio
theme_channel = 0
explosion_channel = 1
victory_channel = 2
button_channel = 3
theme_song = '../music/background_song.ogg'
explosion_sound = '../music/explosion.ogg'
victory_sound = '../music/victory.ogg'


# images
vader = '../images/darth_vader.png'
bg_image = '../images/space.jpg'
logo = '../images/logo.png'
leia = '../images/leia.png'
death_star = '../images/death_star.jpg'
game_bg = '../images/space2.png'
stationary_obstacle = '../images/asteroid.png'
x_wing = '../images/x_wing.png'
obj_bg = '../images/falcon.jpg'
luke = '../images/luke.png'
tie = '../images/tie.png'
han = '../images/han_solo.png'

# menu buttons
mb_w = 350
mb_h = 100

# message dimension
message_x = display_w/1.25
message_y = display_h/1.25

# fonts
font_button_type = '../fonts/button_text.ttf'
font_help_type = '../fonts/help_text.otf'
font_content_type = '../fonts/content_text.otf'

# global variables
list1_x = []
list1_y = []
list2_x = []
list2_y = []
list3_x = []
list3_y = []
delay = 3000
speed = 25
repeat_speed = 100
item_size = 100
enemy_speed = 25
player_size = 100
obstacle_start_x = 10
lane_height = 100
initial_pos_x = display_w/2
initial_pos_y = 10 * lane_height - item_size/2

screen = pygame.display.set_mode((display_w, display_h))

background_image = pygame.image.load(bg_image)
help_background = pygame.image.load(death_star)
game_background = pygame.image.load(game_bg)
obj_background = pygame.image.load(obj_bg)

clock = pygame.time.Clock()

logo_image = pygame.image.load(logo).convert_alpha()
logo_image_rect = logo_image.get_rect()
logo_image_rect.center = (display_w/2, display_h/4)

asteroid = pygame.image.load(stationary_obstacle).convert_alpha()
asteroid = pygame.transform.scale(asteroid, (item_size, item_size))
asteroid_rect = asteroid.get_rect()

x_wing_image = pygame.image.load(x_wing).convert_alpha()
x_wing_image = pygame.transform.scale(x_wing_image, (item_size, item_size))
x_wing_image_rect = x_wing_image.get_rect()

leia_image = pygame.image.load(leia).convert_alpha()
leia_image = pygame.transform.rotozoom(leia_image, 0, 1.5)
leia_image_rect = leia_image.get_rect()

vader_image = pygame.image.load(vader).convert_alpha()
vader_image = pygame.transform.rotozoom(vader_image, 0, 0.75)
vader_image_rect = vader_image.get_rect()
luke_image = pygame.image.load(luke).convert_alpha()
luke_image = pygame.transform.rotozoom(luke_image, 0, 0.75)
luke_image_rect = luke_image.get_rect()

tie_image = pygame.image.load(tie).convert_alpha()
tie_image = pygame.transform.scale(tie_image, (item_size, item_size))
tie_image_rect = tie_image.get_rect()

han_image = pygame.image.load(han).convert_alpha()
han_image = pygame.transform.rotozoom(han_image, 0, 0.75)
han_image_rect = han_image.get_rect()
