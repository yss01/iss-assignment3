from game_config import *
import pygame
import sys
import game_utilities


def intro_page():
    pygame.mixer.Channel(theme_channel).unpause()
    intro_var = False
    screen.blit(background_image, [0, 0])

    while not intro_var:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        screen.blit(logo_image, logo_image_rect)

        mouse = pygame.mouse.get_pos()
        font_button = pygame.font.Font(font_button_type, 60)
        temp = font_button.render("Lorem", True, yellow)

        if display_w/2 + mb_w/2 > mouse[0] > display_w/2 - mb_w/2 and \
                display_h/2.35 + mb_h > mouse[1] > display_h/2.35:
            game_utilities.button_action("Play", font_button, yellow, black,
                                         display_w/2 - mb_w/2, display_h/2.35,
                                         mb_w, mb_h, display_w/2,
                                         display_h/2.35 +
                                         temp.get_height() / 2, 1)
        else:
            game_utilities.button_action("Play", font_button, black, yellow,
                                         display_w/2 - mb_w/2, display_h/2.35,
                                         mb_w, mb_h, display_w/2,
                                         display_h/2.35 +
                                         temp.get_height() / 2, 0)

        if display_w/2 + mb_w/2 > mouse[0] > display_w/2 - mb_w/2 and \
                display_h/1.75 + mb_h > mouse[1] > display_h/1.75:
            game_utilities.button_action("Help", font_button, yellow, black,
                                         display_w/2 - mb_w/2, display_h/1.75,
                                         mb_w, mb_h, display_w/2,
                                         display_h/1.75 +
                                         temp.get_height()/2, 2)
        else:
            game_utilities.button_action("Help", font_button, black, yellow,
                                         display_w/2 - mb_w/2, display_h/1.75,
                                         mb_w, mb_h, display_w/2,
                                         display_h/1.75 +
                                         temp.get_height() / 2, 0)

        pygame.display.update()
        clock.tick(60)
