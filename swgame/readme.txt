---WELCOME TO MY STAR WARS GAME---

Each player gets three chances in total. The maximum number of levels they can complete is 3. Note that it is not 3 chances per LEVEL - rather 3 chances to go as far as the player can.

Once the player 1 either loses all of his chances or completes Level 3, the turn is now Player 2's.

The judgement of winner is done as follows:
1. The player who has gone the farthest (number of levels completed).
2. The total points acquired, in case both complete the same number of levels.

How Points are added:
Awarding points when the player crosses a stationary obstacle is not of much use, because they don't pose much of a challenge.

However, the player still does get points for escaping a moving obstacle. But they aren't fixed points - THE LESSER THE DISTANCE BETWEEN THE ENEMY AND PLAYER'S SHIP JUST BEFORE CROSSING, THE MORE POINTS THE PLAYER RECEIVES. In other words, more risk reaps more rewards.
